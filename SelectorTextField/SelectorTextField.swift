//
//  SelectorTextField.swift
//  SelectorTextField
//
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

@IBDesignable class SelectorTextField: UIView {
    
    private enum Status{
        case opened
        case closed
    }
    
    private var tableView: UITableView!
    private var textField: UITextField!
    private var dropDownArrow:UIImageView!
    private var containerView:UIView!
    private var textViewGesture:UITapGestureRecognizer!
    private var dropDownGesture:UITapGestureRecognizer!
    private var tableHeightConstraint:NSLayoutConstraint!
    private var containerHeightConstraint:NSLayoutConstraint!
   
    private var textFieldHeightConstraint:NSLayoutConstraint!
    private var textFieldTopConstraint:NSLayoutConstraint!
    
    private var borderLayer:CALayer!
    
    private var status:Status!
    private var path:UIBezierPath!
    
    private var tableViewData:[String] = ["Basketball", "Football", "Soccer", "Baseball" , "Hockey"]
    
    private var textFieldHeight:CGFloat = 40.0
    private var cellHeight:CGFloat = 40.0
    
    override var intrinsicContentSize: CGSize {
        return CGSize.init(width: self.bounds.width, height: self.textFieldHeight + (CGFloat(tableViewData.count) * cellHeight))
    }
    
    
    @IBInspectable var arrowColor:UIColor = UIColor.black{
        
        didSet{
            dropDownArrow.tintColor = arrowColor
        }
    }
    
    @IBInspectable var initialText:String = "Select a Sport"{
        
        didSet {
            textField.text = initialText
        }
    }
    
    @IBInspectable var selectorTextColor:UIColor = UIColor.black{
        
        didSet{
            textField.textColor = selectorTextColor
            
        }
    }
    
    
    @IBInspectable var selectorBackgroundColor:UIColor = UIColor.init(hexString: "ebedf0"){
        
        didSet{
            containerView.backgroundColor = selectorBackgroundColor
            tableView.backgroundColor = selectorBackgroundColor
        }
    }
    
    
    @IBInspectable var dividerEnabled:Bool = true{
        
        didSet {
            if dividerEnabled{
                containerView.layoutIfNeeded()
                borderLayer = containerView.layer.addBorder(edge: .bottom, color: dividerColor, thickness: dividerSize)
            }else{
                borderLayer.removeFromSuperlayer()
                containerView.setNeedsLayout()
            }
        }
    }
    
    
    @IBInspectable var dividerSize:CGFloat = 3.0{
        
        didSet {
            if dividerEnabled{
                borderLayer.removeFromSuperlayer()
                containerView.layoutIfNeeded()
                borderLayer = containerView.layer.addBorder(edge: .bottom, color: dividerColor, thickness: dividerSize)
                containerView.setNeedsLayout()
            }
            
        }
    }
    
    
    @IBInspectable var dividerColor:UIColor = UIColor.darkGray{
        
        didSet {
           
            if dividerEnabled{
                borderLayer.backgroundColor = dividerColor.cgColor
                containerView.setNeedsLayout()
            }
        }
    }
    
    
    @IBInspectable var selectorFontSize:CGFloat = 18.0{
        
        didSet {
            self.textField.font = UIFont.systemFont(ofSize: selectorFontSize, weight: UIFont.Weight.medium)
        }
    }
    
   
    
    override init(frame: CGRect) {
        
        super.init(frame:frame)
        
        self.bounds.size = CGSize(width: self.frame.width, height: self.bounds.height + (CGFloat(tableViewData.count) * cellHeight))
      
        initializeComponents()
        positionComponents()
        
        textViewGesture = UITapGestureRecognizer.init(target: self, action:  #selector(triggerTable))
        textField.addGestureRecognizer(textViewGesture)
        dropDownGesture = UITapGestureRecognizer.init(target: self, action:  #selector(triggerTable))
        dropDownArrow.addGestureRecognizer(dropDownGesture)
        
        status = Status.closed
       
       
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    
        self.bounds.size = CGSize(width: self.frame.width, height: self.bounds.height + (CGFloat(tableViewData.count) * cellHeight))
    
        initializeComponents()
        positionComponents()
       
        textViewGesture = UITapGestureRecognizer.init(target: self, action:  #selector(triggerTable))
        textField.addGestureRecognizer(textViewGesture)
        dropDownGesture = UITapGestureRecognizer.init(target: self, action:  #selector(triggerTable))
        dropDownArrow.addGestureRecognizer(dropDownGesture)
       
        status = Status.closed
        
    }
   
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    
    override func prepareForInterfaceBuilder() {
        
        super.prepareForInterfaceBuilder()
       
        textField.contentVerticalAlignment = .fill
        
        if selectorFontSize <= 10{
            textFieldTopConstraint.constant = 13.0
        }else if selectorFontSize > 10 && selectorFontSize <= 15{
            textFieldTopConstraint.constant = 10.0
        }else if selectorFontSize > 15 && selectorFontSize <= 20{
            textFieldTopConstraint.constant = 7.0
        }else if selectorFontSize > 20 && selectorFontSize <= 30{
            textFieldTopConstraint.constant = 5.0
        }
        
        let dynamicBundle = Bundle(for: type(of: self))
        let img = UIImage(named: "dropdown", in: dynamicBundle, compatibleWith: nil)
        dropDownArrow.image = img

        if dividerEnabled{
            containerView.layoutIfNeeded()
            borderLayer = containerView.layer.addBorder(edge: .bottom, color: dividerColor, thickness: dividerSize)
            containerView.setNeedsLayout()
        }
    }

}

//MARK:- Initializers
extension SelectorTextField{
    
    private func initializeComponents(){
        
        // Tableview
        tableView = UITableView(frame: CGRect.zero)
        tableView.register(SelectorCell.self, forCellReuseIdentifier: "selectorcell")
        tableView.bounces = false
        tableView.backgroundColor = selectorBackgroundColor
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.separatorColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 1, height: 1))
        
        self.addSubview(tableView)
       
        //Container
        containerView = UIView(frame:.zero)
        
        //TextField
        textField = UITextField(frame: .zero)
        textField.text = initialText
        textField.font = UIFont.systemFont(ofSize: selectorFontSize, weight: UIFont.Weight.medium)
        containerView.addSubview(textField)
        
        //Dropdown Arrow
        dropDownArrow = UIImageView.init(frame: .zero)
        dropDownArrow.image = UIImage(named: "dropdown")
        dropDownArrow.isUserInteractionEnabled = true
        containerView.addSubview(dropDownArrow)
        dropDownArrow.tintColor = arrowColor
        
        self.addSubview(containerView)
    }
    
    
    private func positionComponents(){
        
        //Container
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant:10.0).isActive = true
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant:-10.0).isActive = true
        containerHeightConstraint = containerView.heightAnchor.constraint(equalToConstant: textFieldHeight)
        containerHeightConstraint.isActive = true
        
        
        //Textfield
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant:10.0).isActive = true
        textFieldTopConstraint = textField.topAnchor.constraint(equalTo: containerView.topAnchor)
        textFieldTopConstraint.isActive = true
        textField.trailingAnchor.constraint(equalTo: dropDownArrow.leadingAnchor).isActive = true
        textFieldHeightConstraint = textField.heightAnchor.constraint(equalToConstant: textFieldHeight)
        textFieldHeightConstraint.isActive = true
        
        //Dropdown
        let arrowWidthHeight = cellHeight/2
        dropDownArrow.translatesAutoresizingMaskIntoConstraints = false
        dropDownArrow.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant:-10.0).isActive = true
        dropDownArrow.topAnchor.constraint(equalTo: containerView.topAnchor, constant:arrowWidthHeight/2).isActive = true
        dropDownArrow.heightAnchor.constraint(equalToConstant: arrowWidthHeight).isActive = true
        dropDownArrow.widthAnchor.constraint(equalToConstant: arrowWidthHeight).isActive = true
        
        //Tableview
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant:15.0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant:-15.0).isActive = true
        tableView.topAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
       
        // Round the edges of the dropdown table
        tableView.clipsToBounds = true
        tableView.layer.cornerRadius = 10
        tableView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        tableHeightConstraint = tableView.heightAnchor.constraint(equalToConstant: 0.0)
        tableHeightConstraint.isActive = true

       
        //Add border to container
        if dividerEnabled{
            containerView.layoutIfNeeded()
            borderLayer = containerView.layer.addBorder(edge: .bottom, color: UIColor.init(hexString: "cce0ff"), thickness: dividerSize)
       
        }
        
    }

}


//MARK:- TableViewDelegate & TableViewDataSource
extension SelectorTextField: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if status == .closed{
            return 0
        }else{
            return tableViewData.count
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectorcell") as? SelectorCell ?? SelectorCell()
      
        cell.textLabel?.text = self.tableViewData[indexPath.row]
        cell.textLabel?.textColor = selectorTextColor
        cell.textLabel?.font = UIFont.systemFont(ofSize: selectorFontSize, weight: UIFont.Weight.light)
        cell.backgroundColor = selectorBackgroundColor
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.textField.text = tableViewData[indexPath.row]
        triggerTable()
    }
    
}

extension SelectorTextField{
    
    @objc private func triggerTable(){
       
        tableView.backgroundColor = selectorBackgroundColor
        
        if status == .closed{
            status = .opened
       
            UIView.animate(withDuration: 0.7, delay:0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations:{
                self.tableHeightConstraint.constant +=  self.cellHeight * CGFloat(self.tableViewData.count)
                self.dropDownArrow.transform = CGAffineTransform(rotationAngle: .pi)
                self.layoutIfNeeded()
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(300), execute: {
                    self.tableView.separatorColor = self.dividerColor
                    self.tableView.reloadData()
                })
            }){_ in
                //Remove DispatchQueue above and uncomment this when changing aniumation
                //to not use springVelocity and Damping
                //self.tableView.separatorColor = UIColor.darkGray
                //self.tableView.reloadData()
            }
            
            
        }else{
            
            status = .closed
            self.tableView.reloadData()
            UIView.animate(withDuration: 0.7, delay:0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations:{
                
                self.tableHeightConstraint.constant -=  self.cellHeight * CGFloat(self.tableViewData.count)
                self.dropDownArrow.transform = CGAffineTransform(rotationAngle: .pi - 3.14159)
                self.tableView.separatorColor = UIColor.clear
                self.layoutIfNeeded()
                
            })
        }

    }
}


//MARK:- CALayer Extension
// From Stackoverflow:
// https://stackoverflow.com/questions/17355280/how-to-add-a-border-just-on-the-top-side-of-a-uiview
extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) -> CALayer{
        
        let border = CALayer()
        
        switch edge {
        case .top:
            border.frame = CGRect(x: 0, y: 0, width: frame.width, height: thickness)
        case .bottom:
            border.frame = CGRect(x: 0, y: frame.height - thickness, width: self.bounds.size.width, height: thickness)
        case .left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: frame.height)
        case .right:
            border.frame = CGRect(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        addSublayer(border)
        
        return border
    }
    
}


public extension UIColor {
    
    convenience init(hexString:String) {

        let hexString:String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    
}

